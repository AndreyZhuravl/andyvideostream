package com.andyvideostream.andyvideostream;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;


public class MainActivity extends ActionBarActivity {
    static final int REQUEST_VIDEO_CAPTURE = 1;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.videoButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = data.getData();
            File fileVideo = new File(getRealPathFromURI(videoUri));
            if (fileVideo.exists()) {
                progress = ProgressDialog.show(this, "Uploading",
                        "Upload file to the server! Please wait!" + "\nFile size = " + fileVideo.length()/1000 + " Kb", true);

                Ion.with(MainActivity.this)
                        .load("http://usc-sample-data.herokuapp.com/tasks.json")
                        .setMultipartParameter("task[name]", fileVideo.getName())
                        .setMultipartFile("filedata", fileVideo)
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String s) {
                        if (progress != null)
                            progress.dismiss();
                        Toast.makeText(getApplicationContext(), "Done!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Video.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}